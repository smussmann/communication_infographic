type_map = {2: 'voicemail',
            0: 'missed-call',
            10: 'sms',
            11: 'sms',
            1: 'received-call',
            9: 'received-call',
            7: 'placed-call',
            8: 'placed-call',
            12: 'placed-call',
            13: 'placed-call',
            14: 'placed-call',
            15: 'placed-call',
            16: 'placed-call',
            17: 'placed-call'};
var url = document.location.hash;
var out_list = Array();
var messages = document.getElementsByClassName('gc-message-tbl');
for (var i = 0; i < messages.length; i++) {
    var message = messages[i];
    var msg_out = {};
    msg_out.time = message.getElementsByClassName('gc-message-time')[0].textContent;
    msg_out.duration = message.getElementsByClassName('gc-message-call-details')[0];
    if (msg_out.duration) msg_out.duration = msg_out.duration.textContent;
    msg_out.raw_type = message.getElementsByClassName('gc-message-portrait')[0].lastElementChild.className.split('-')[3];
    msg_out.type = type_map[msg_out.raw_type];
    if (msg_out.type == 'sms') {
        msg_out.times = Array();
        var times = message.getElementsByClassName('gc-message-sms-time');
        for (var j = 0; j < times.length; j++) {
            msg_out.times.push(times[j].innerText);
        }
    }
    out_list.push(msg_out);
}
console.log('next round');
if (/.*p[0-9]+/.test(url)) {
    var page_num_idx = url.search(/\d+$/);
    var page_num = url.slice(page_num_idx);
    url = url.slice(0, page_num_idx) + (parseInt(page_num) + 1);
    document.location.hash = url;
} else {
    document.location.hash = url + '/p2';
}
chrome.extension.sendRequest(out_list);
var xhr = new XMLHttpRequest();
xhr.open("POST", "http://localhost:8080/", false)
xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
xhr.send(JSON.stringify(out_list));

console.log(xhr.responseText);
