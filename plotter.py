import datetime
import time

def date_to_timestamp(date):
    return int(time.mktime(date.timetuple()))

class OutOfBoundsError(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return repr(self.value)

pixels_per_column = 10
column_height = 1000
start_date = datetime.datetime(year=2011, month=9, day=1)
start_date_timestamp = date_to_timestamp(start_date)
end_date = datetime.datetime(year=2012, month=7, day=1)

def datetime_to_coords(date, mod=datetime.timedelta(days=1)):
    if date > end_date:
        raise OutOfBoundsError('Date is too late')
    if date < start_date:
        raise OutOfBoundsError('Date is too early')
    mod_in_seconds = mod.days * (24 * 3600) + mod.seconds
    timestamp = date_to_timestamp(date) - start_date_timestamp
    x_logical = timestamp / mod_in_seconds
    x_raw = x_logical * pixels_per_column + pixels_per_column / 2
    y_logical = timestamp % mod_in_seconds
    y_raw = column_height * y_logical / mod_in_seconds
    return (x_raw, y_raw)
    
