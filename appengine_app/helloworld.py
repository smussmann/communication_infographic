import datetime
import jinja2
import json
import os
import webapp2

from google.appengine.ext import db
from google.appengine.api import users

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.dirname(__file__)))

class PhoneCall(db.Model):
    start_time = db.DateTimeProperty()
    end_time = db.DateTimeProperty()
    def to_dict(self):
        return {'type': 'phone-call',
                'start_time': str(self.start_time),
                'end_time': str(self.end_time), }


class Sms(db.Model):
    sent_time = db.DateTimeProperty()
    def to_dict(self):
        return {'type': 'sms',
                'sent_time': str(self.sent_time), }

class MyUser(db.Model):
    google_user = db.UserProperty()
    def nickname(self):
        return self.google_user.nickname()

def parse_time(item):
    return datetime.datetime.strptime(item['time'], '%m/%d/%y %I:%M %p')

def parse_sms_time(item, time):
    date = item['time'].split()[0]
    return datetime.datetime.strptime(' '.join([date, time]),
                                      '%m/%d/%y %I:%M %p')

def parse_duration(item):
    return datetime.timedelta(minutes = int(item['duration'].split()[0]))

def init_user():
    user = users.get_current_user()
    if not user:
        return None
    my_user = MyUser.get(db.Key.from_path('MyUser', user.user_id()))
    if not my_user:
        my_user = MyUser(key_name=user.user_id())
        my_user.google_user = user
        my_user.put()
    return my_user

class Stats(webapp2.RequestHandler):
    def get(self):
        user = init_user()
        if not user:
            self.redirect(users.create_login_url(self.request.uri))
            return

        durations = [c.end_time - c.start_time for c in PhoneCall.all().ancestor(user).order('start_time')]
        durations.sort()
        sum_durations = sum(durations, datetime.timedelta())
        def avg_duration(duration_list):
            sum_duration_list = sum(duration_list, datetime.timedelta())
            return datetime.timedelta(seconds=sum_duration_list.total_seconds() / len(duration_list))


        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write('total: %s\n' % str(sum_durations))
        self.response.out.write('#: %d\n' % len(durations))
        self.response.out.write('mean: %s\n' % str(avg_duration(durations)))
        self.response.out.write('median: %s\n' % durations[len(durations) / 2])
        self.response.out.write('max: %s\n' % durations[-1])
        long_durations = filter(lambda x: x > datetime.timedelta(minutes=5), durations)
        long_durations.sort()
        self.response.out.write('# >5: %d\n' % len(long_durations))
        self.response.out.write('mean >5: %s\n' % str(avg_duration(long_durations)))
        self.response.out.write('median >5: %s\n' % long_durations[len(long_durations) / 2])
        

class Deleter(webapp2.RequestHandler):
    def get(self):
        user = init_user()
        if not user:
            self.redirect(users.create_login_url(self.request.uri))
            return
        self.response.out.write('''<form method='POST'>Do you want to delete all your data?
          <input type='submit' action='Yes!'></form>''')
    def post(self):
        user = init_user()
        if not user:
            self.redirect(users.create_login_url(self.request.uri))
            return
        for call in PhoneCall.all().ancestor(user).order('start_time'):
            call.delete()
        for sms in Sms.all().ancestor(user).order('sent_time'):
            sms.delete()
        self.redirect('/')

class RawAccess(webapp2.RequestHandler):
    def get(self):
        user = init_user()
        if not user:
            self.redirect(users.create_login_url(self.request.uri))
            return
        
        data = {
            'calls': map(lambda x: x.to_dict(), list(PhoneCall.all().ancestor(user).order('start_time'))),
            'sms': map(lambda x: x.to_dict(), list(Sms.all().ancestor(user).order('sent_time'))),
        }
        self.response.headers['Content-Type'] = 'text/plain'
        self.response.out.write(json.dumps(data))

class MainPage(webapp2.RequestHandler):
    def get(self):
        user = init_user()
        if not user:
            self.redirect(users.create_login_url(self.request.uri))
            return
        template_values = {
            'calls': PhoneCall.all().ancestor(user).order('start_time'),
            'sms': Sms.all().ancestor(user).order('sent_time'),
            'user': user,
        }
        template = jinja_environment.get_template('index.html')
        self.response.out.write(template.render(template_values))

    def post(self):
        user = init_user()
        if not user:
            self.redirect('/')
            return
        for item in json.loads(self.request.arguments()[0]):
            if item['type'] == 'sms':
                for time in item['times']:
                    sms = Sms(parent=user)
                    sms.sent_time = parse_sms_time(item, time)
                    sms.put()
                    self.response.out.write(sms)
            if item['type'] == 'received-call' or item['type'] == 'placed-call':
                call = PhoneCall(parent=user)
                call.start_time = parse_time(item)
                call.end_time = call.start_time + parse_duration(item)
                call.put()
                self.response.out.write(call)


app = webapp2.WSGIApplication([('/', MainPage),
                               ('/raw', RawAccess),
                               ('/delete', Deleter),
                               ('/stats', Stats),
                               ], debug=True)

