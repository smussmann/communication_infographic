import imaplib
import email
import pprint
import re

mail = imaplib.IMAP4_SSL('imap.gmail.com')
password = 'nvlppmxgghbwcbbc'
mail.login('sdmuss@gmail.com', password)
mail.capability()
mail.select('[Gmail]/Chats', readonly=True)
result, data = mail.search(None, 'X-GM-RAW', 'from:annabookreader@gmail.com OR from:anna.beck@immanuelalexandria.org')
result, messages = mail.fetch(','.join(data[0].split()), '(BODY[HEADER.FIELDS (DATE)])')
messages = filter(lambda s: s != ')', messages)
chats = map(lambda t: t[1], messages)

class Message(object):
    def __init__(self, header_string):
        headers = filter(lambda x: x != '', header_string.split('\r\n'))
        self.addresses = re.findall(r'<([^>]*)>', ' '.join(headers))
        self.date = header_string.split('\r\n', 1)[0]

    def has_only_addresses(self, addresses):
        return len(filter(lambda x: x not in addresses, self.addresses)) == 0
       

email_addresses = ['annabookreader@gmail.com', 'anna.beck@immanuelalexandria.org', 'ilonabeck@yahoo.com']
def from_and_to(email_addresses):
	result = []
	for email in email_addresses:
		result.append('from:' + email)
		result.append('to:' + email)
	return ' OR '.join(result)
mail.select('[Gmail]/All Mail')
result, data = mail.search(None, 'X-GM-RAW', from_and_to(email_addresses))
result, messages = mail.fetch(','.join(data[0].split()), '(BODY[HEADER.FIELDS (DATE FROM TO)])')
messages = filter(lambda s: s != ')', messages)
messages = map(lambda t: Message(t[1]), messages)
acceptable_addresses = email_addresses + ['sdmuss@gmail.com', 'sam@sammussmann.net']
emails = map(lambda m: m.date, filter(lambda m: m.has_only_addresses(acceptable_addresses), messages))
